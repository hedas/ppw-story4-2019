from django.apps import AppConfig


class BiografiConfig(AppConfig):
    name = 'biografi'
