from django.urls import path

from . import views

urlpatterns = [
    path('', views.biografi),
    path('contact/', views.contact),
    path('contact/saran/', views.saran),
    path('ability/', views.ability),
    path('experience/', views.experience),
    path('aboutme/', views.aboutme),
]

