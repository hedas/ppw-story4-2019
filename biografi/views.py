from django.shortcuts import render

# Create your views here.

def biografi(request):
    return render(request, 'index.html')

def contact(request):
    return render(request, "contact.html")

def ability(request):
    return render(request, "ability.html")

def experience(request):
    return render(request, "experience.html")

def aboutme(request):
    return render(request, "aboutme.html")

def saran(request):
    return render(request, "saran.html")